# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
import os


def insert_sorted(path: str, value: str):
    """
    Inserts the given value into a list file, preserving sort order and comments
    """
    if os.path.exists(path):
        with open(path, encoding="utf-8") as file:
            contents = file.readlines()
        for index, line in enumerate(contents):
            if line.strip() < value or line.startswith("#"):
                pass
            elif line.strip() >= value:
                contents.insert(index, value + os.linesep)
                break
    else:
        contents = [value]
    with open(path, "w", encoding="utf-8") as file:
        file.writelines(contents)
