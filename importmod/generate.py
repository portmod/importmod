"""
Module for generating pybuilds
"""

import datetime
import os
import re
import urllib
from collections import defaultdict
from contextlib import contextmanager
from logging import error, warning
from types import SimpleNamespace
from typing import Any, Callable, Dict, Iterable, List, Optional, Tuple, cast

import black
import isort
import ruamel.yaml.comments
from colorama import Fore
from portmod._cli.pybuild import create_manifest
from portmod.download import download, get_download
from portmod.globals import env
from portmod.loader import load_all, load_file, load_pkg
from portmod.prompt import prompt_num_multi
from portmod.pybuild import Pybuild
from portmod.repo import Repo, get_repo, get_repo_root
from portmod.repo.metadata import get_categories, get_category_metadata
from portmod.repo.metadata import get_masters as get_repo_masters
from portmod.repo.metadata import license_exists
from portmodlib.atom import Atom, Version, version_gt
from portmodlib.colour import colour
from portmodlib.usestr import use_reduce
from prompt_toolkit.validation import ValidationError, Validator
from redbaron import AtomtrailersNode, CallNode, RedBaron
from ruamel.yaml import YAML

from importmod_core.install import unpack
from importmod_core.io import insert_sorted
from importmod_core.prompt import confirm, prompt_default

from .atom import parse_atom
from .license import detect_license
from .sources import PackageData, PackageSource
from .update import get_newest, get_pkg_sources, guess_package_source, should_copy


def get_first_attr(ns: SimpleNamespace):
    return list(ns.__dict__)[0]


LOCAL_REPO = Repo("local", os.path.join(env.REPOS_DIR, "local"), False, None, None, 50)


def try_find(node, attr, key):
    try:
        return node.find(attr, key)
    except ValueError:
        return None


def update_string(idir: RedBaron, new_idir: object, key: str):
    if hasattr(new_idir, key) and try_find(idir, "name", key):
        idir.find("name", key).parent.value = f'"{getattr(new_idir, key)}"'
    elif hasattr(new_idir, key) and getattr(new_idir, key):
        idir.append(f'{key}="{getattr(new_idir, key)}"')
    elif try_find(idir, "name", key):
        idir.remove(idir.find("name", key).parent)


def update_namespace(idir: AtomtrailersNode, new_idir: SimpleNamespace):
    """Updates idir to match new_idir"""
    assert idir.value[0].value == new_idir.__class__.__name__
    assert isinstance(idir.value[1], CallNode)
    inner = idir.value[1]

    for key in dir(new_idir):
        if key.startswith("_"):
            continue

        if key == get_first_attr(new_idir):
            continue

        value = getattr(new_idir, key)
        if isinstance(value, str):
            update_string(inner, new_idir, key)
        elif isinstance(value, list):
            if hasattr(new_idir, key) and inner.find("name", key):
                pending_files = {file.NAME: file for file in getattr(new_idir, key)}
                list_for_key = inner.find("name", key).parent.value

                files = [
                    file for file in list_for_key if isinstance(file, AtomtrailersNode)
                ]

                # If there is only one plugin, assume it is the same as the old one
                if len(files) == 1 and len(value) == 1:
                    update_namespace(files[0], value[0])
                else:
                    # Otherwise, leave any old files that match the new files names,
                    # remove files that aren't in the new list,
                    # and add any missing files to the list
                    for file in files:
                        # File name
                        name = file.value[1].value[0]
                        if name in pending_files:
                            del pending_files[name]
                        else:
                            list_for_key.remove(file)

                    for file in pending_files:
                        list_for_key.node_list.append(
                            RedBaron(str(pending_files[file]) + ",")
                        )
            elif inner.find("name", key):
                # New idir doesn't have this element. Delete the old list
                inner.remove(inner.find("name", key).parent)
            elif hasattr(new_idir, key) and getattr(new_idir, key):
                inner.append(f"{key}={getattr(new_idir, key)}")


def get_header_string(end: int, start: Optional[int] = None):
    # Copyright Header
    if start:
        copyright_string = f"{start}-{end}"
    else:
        copyright_string = str(end)
    return [
        f"# Copyright {copyright_string} Portmod Authors",
        "# Distributed under the terms of the GNU General Public License v3",
    ]


YEAR = datetime.datetime.now().year


def get_old_package(package: PackageData) -> Optional[Pybuild]:
    if package.atom.C:
        oldmods = load_pkg(Atom(package.atom.CPN))
    else:
        oldmods = load_pkg(Atom(package.atom.PN))
    if oldmods:
        newest_package = get_newest(oldmods)
        return newest_package

    return None


class CategoryValidator(Validator):
    def validate(self, document):
        text = document.text
        if not text:
            raise ValidationError(message="Categories must have a non-zero length")

        if not text[0].isalpha():
            raise ValidationError(
                message="Categories must begin with an alphanumeric character"
            )
        if text and not re.match(r"^[A-Za-z0-9][A-Za-z0-9-]*$", text):
            i = 0

            # Get index of first non numeric character.
            # We want to move the cursor here.
            for i, c in enumerate(text):
                if not c.isalnum() and not c == "-":
                    break

            raise ValidationError(
                message="Categories can only contain alphanumeric characters and hyphens",
                cursor_position=i,
            )


class AtomValidator(Validator):
    def validate(self, document):
        text = document.text
        if not text:
            raise ValidationError(message="Atoms must have a non-zero length")

        if not text[0].isalpha():
            raise ValidationError(
                message="Atoms must begin with an alphanumeric character"
            )
        if text and not re.match(r"^[A-Za-z0-9][A-Za-z0-9_+-]*$", text):
            i = 0

            # Get index of first non numeric character.
            # We want to move the cursor here.
            for i, c in enumerate(text):
                if not c.isalnum() and not c == "-":
                    break

            raise ValidationError(
                message="Categories can only contain alphanumeric characters and hyphens",
                cursor_position=i,
            )


class VersionValidator(Validator):
    def validate(self, document):
        text = document.text
        if not text:
            raise ValidationError(message="Versions must have a non-zero length")

        def try_parse(version: str):
            try:
                Version(version)
                return True
            except (TypeError, ValueError):
                return False

        if text and not try_parse(text):
            i = 0

            # Get index of first non numeric character.
            # We want to move the cursor here.
            for i, c in enumerate(text):
                if not c.isalnum() and not c == "-":
                    break

            raise ValidationError(
                message="Invalid version. Check docs for format details",
                cursor_position=i,
            )


def get_source_data(mod) -> Tuple[PackageData, PackageSource, Optional[Pybuild]]:
    def get_atom(mod):
        if "atom" in mod:
            atom = Atom(mod["atom"])
        elif "category" in mod and "name" in mod:
            atom = parse_atom(mod["category"] + "/" + mod["name"])
        else:
            atom = None
        return atom

    url = mod.get("url")

    package = PackageData(
        atom=get_atom(mod),
        name=mod.get("name"),
        desc=mod.get("desc") or mod.get("description"),
        homepage=mod.get("homepage"),
        classes=[],
        imports=defaultdict(set),
        src_uri=url,
    )
    if "author" in mod:
        package.authors.append(mod.get("author"))

    upstream_source = None
    if url:
        upstream_source = guess_package_source(url)
        if upstream_source:
            upstream_source.get_pkg_data(package)

    oldpkg = None
    if package.atom:
        oldpkg = get_old_package(package)
        if not upstream_source:
            for possible_source in get_pkg_sources(oldpkg, no_implicit=False):
                if possible_source.get_pkg_data(package):
                    upstream_source = possible_source
                    break

    assert (
        upstream_source is not None
    ), "Unable to find an upstream mod source for this package!"

    if oldpkg:
        package.category = oldpkg.CATEGORY
        package.desc = oldpkg.DESC
        package.license = oldpkg.LICENSE

    if oldpkg and package.atom.PV:
        package.atom = Atom(oldpkg.CPN + "-" + package.atom.PV)
    elif oldpkg:
        package.atom = Atom(oldpkg.CPN)

    return package, upstream_source, oldpkg


def generate_build_files(mod, *, noreplace=False, repo="local"):
    """
    Generates pybuilds from a mod decription dictionary.

    Valid Fields: atom, name, desc, homepage, category, url, file,
      author, needs_cleaning
    Other fields are ignored
    """
    package, upstream_source, oldpkg = get_source_data(mod)

    if repo == "local":
        REPO = LOCAL_REPO
    else:
        REPO = get_repo(repo)

    print("Auto-detected information:")
    package.pretty_print()
    if os.environ.get("INTERACTIVE"):
        print("Starting interactive package generation")
        print()

    if package.atom and package.atom.C:
        package.category = package.atom.C

    if not package.category and os.environ.get("INTERACTIVE"):
        categories = get_categories(REPO.location)
        category_metadata = {
            category: get_category_metadata(REPO.location, category)
            for category in categories
        }
        category_descriptions = {
            category: metadata.longdescription
            for category, metadata in category_metadata.items()
            if metadata is not None
        }
        package.category = prompt_default(
            "<b>Package Category</b> (type to auto-complete): ",
            package.category,
            options=list(categories),
            options_desc=category_descriptions,
            validator=CategoryValidator(),
        )
        if package.category not in categories:
            warning(
                f"The chosen category does not exist. This will create a new category with the name {package.category}"
            )
            if not package.category.islower():
                warning(
                    "It is strongly recommended that categories are always lowercase"
                )
            if confirm("Would you like to continue?"):
                path = os.path.join(REPO.location, "profiles", "categories")
                os.makedirs(os.path.dirname(path), exist_ok=True)
                insert_sorted(path, package.category)

    if not package.category:
        raise RuntimeError(
            "No category could be determined. Either manually specify a category, or run in interactive mode"
        )
    package.atom = Atom(
        f"{package.category}/"
        + prompt_default(
            "<b>Package Name</b> (slug/atom): ",
            package.atom.PN if package.atom else None,
            validator=AtomValidator(),
        )
        + "-"
        + prompt_default(
            "<b>Package Version: </b>",
            package.atom.PVR if package.atom else None,
            validator=VersionValidator(),
        )
    )

    if noreplace and oldpkg and not version_gt(oldpkg.PVR, package.atom.PVR):
        print(f"Package {package.atom} already exists. Skipping...")
        return

    assert package.atom
    C = package.category
    assert C, "A category must be set!"
    P = package.atom.P
    PN = package.atom.PN

    if not os.environ.get("INTERACTIVE"):
        print(f"Importing {package.atom}...")

    build_file: Optional[str]
    if oldpkg and should_copy(oldpkg):
        print(f"Using file from {oldpkg}")
        with open(oldpkg.FILE) as file:
            pybuild = RedBaron(file.read())
            # KEYWORDS still need to be modified
            clear_keywords(pybuild)
            build_file = pybuild.dumps()
    else:
        build_file = generate_pybuild(
            package, upstream_source, oldpkg, mod.get("needs_cleaning"), REPO
        )
    if build_file is None:
        return

    # User import repo may not exist. If not, create it
    if not os.path.exists(REPO.location):
        os.makedirs(os.path.join(REPO.location, "profiles"), exist_ok=True)
        metadata_file = os.path.join(REPO.location, "profiles", "repo_name")
        with open(metadata_file, "w") as file:
            print("local", file=file)

        layout_file = os.path.join(REPO.location, "metadata", "layout.conf")
        os.makedirs(os.path.dirname(layout_file))
        with open(layout_file, "w") as file:
            print('masters="openmw"', file=file)
        # Add user repo to REPOS so that it can be used in further dependency resolution
        env.REPOS.append(REPO)
        # Write user import repo to repos.cfg
        with open(env.REPOS_FILE, "a") as file:
            userstring = """
[local]
location = {}
auto_sync = False
masters = openmw
priority = 50
"""
            print(userstring.format(REPO.location), file=file)

    if C not in get_categories(REPO.location):
        with open(
            os.path.join(REPO.location, "profiles", "categories"), "a"
        ) as categories:
            print(C, file=categories)

    outdir = os.path.join(REPO.location, C, PN)
    filename = os.path.join(outdir, P + ".pybuild")
    os.makedirs(outdir, exist_ok=True)

    print(f"Writing package file to {filename}")
    with open(filename, "w") as file:
        print(build_file, file=file, end="")

    # Add author to metadata.yaml if provided
    if package.authors:
        create_metadata(
            os.path.join(outdir, "metadata.yaml"),
            authors=package.authors,
            bugs_to=package.bugs_to,
            doc=package.doc,
            sources=[upstream_source],
            longdescription=package.longdescription,
            changelog=package.changelog,
            tags=package.tags,
        )

    # Create manifest file
    try:
        create_manifest(load_file(filename))
    except Exception as e:
        # Manifest generation failed. We need to roll back changes
        error(
            "Encountered error during manifest generation. Removing generated package file..."
        )
        os.remove(filename)
        raise e

    print(colour(Fore.GREEN, f"Finished Importing {package.atom}"))


def clear_keywords(file: RedBaron):
    Package = file.find("class", "Package")

    if Package.find("name", "KEYWORDS"):
        Package.find("name", "KEYWORDS").parent.value = '""'


def generate_pybuild(
    package: PackageData,
    upstream_source: PackageSource,
    oldpkg: Pybuild,
    needs_cleaning: bool,
    repo: Repo,
) -> Optional[str]:
    """Produces a pybuild file in the form of a string"""
    for source in package.sources:
        parsed = urllib.parse.urlparse(source.url)
        if parsed.scheme and not get_download(source):
            download(source.url, source.name)

    if not all([get_download(source) for source in package.sources]):
        if not env.INTERACTIVE:
            print(f"Skipping update to pacakge {package.atom} in non-interactive mode")
            return None
        print("Please download the following files from the url at the bottom")
        print("before continuing and move them to the download directory:")
        print(f"  {env.DOWNLOAD_DIR}")
        print()
        for source in package.sources:
            if not get_download(source):
                print(f"  {source}")
        print()
        assert (
            package.manual_download_url
        ), "PackageData.manual_download_url must be specified if the sources are not fetchable"
        print("  " + package.manual_download_url)
        if not confirm("Continue?"):
            return None

    upstream_source.validate_downloads(package.sources)

    if oldpkg is not None:
        with open(oldpkg.FILE, "r", encoding="utf-8") as pybuild_file:
            pybuild = RedBaron(pybuild_file.read())
    else:
        pybuild = RedBaron("\n".join(get_header_string(YEAR)))

    cleanr = re.compile("<.*?>")
    if package.desc is not None:
        package.desc = re.sub(cleanr, "", package.desc)
        package.desc = (
            package.desc.replace("\n", " ").replace("\r", " ").replace('"', '\\"')
        )

    package.name = prompt_default("<b>Package Name</b> (pretty format): ", package.name)
    package.desc = prompt_default("<b>Package Description:</b> ", package.desc)
    package.homepage = prompt_default("<b>Package Homepage:</b> ", package.homepage)

    def get_all_licenses(repo: Repo):
        repo_path = repo.location
        repos = [repo] + get_repo_masters(repo_path)

        for repo in repos:
            license_path = os.path.join(repo.location, "licenses")
            if os.path.exists(license_path):
                yield from os.listdir(license_path)

    extracted_paths = []
    for source in package.sources:
        # Extract file into tmp
        outname, _ = os.path.splitext(source.name)
        if outname.endswith(".tar"):
            outname, _ = os.path.splitext(outname)
        outdir = os.path.join(env.TMP_DIR, outname)

        os.makedirs(outdir, exist_ok=True)
        unpack(get_download(source), outdir)
        extracted_paths.append(outdir)

        for root, directories, files in os.walk(outdir):
            for file in files:
                if file.lower().startswith("license"):
                    license = detect_license(repo, os.path.join(root, file))
                    if license is not None:
                        package.license = license

    package.license = prompt_default(
        "Package License (type to auto-complete): ",
        package.license or "FILLME",
        options=list(get_all_licenses(repo)),
    )
    if os.environ.get("INTERACTIVE") and package.license != "FILLME":
        for license in use_reduce(package.license, flat=True, matchall=True):
            if not license_exists(repo.location, license):
                warning(
                    f"License {license} does not exist, or is not accessible from the {repo.name} repository! {os.linesep}"
                    "You must create a license in the repository licenses directory for this license before the package will pass QA checks."
                )

    possible_templates: List[Tuple[str, Callable[[List[str], PackageData], None]]] = []
    dirname = os.path.join(os.path.dirname(__file__), "templates")
    for path in os.listdir(dirname):
        if path.startswith("_"):
            continue
        with open(os.path.join(dirname, path)) as file:
            file_globals: Dict[str, Any] = {}
            bytecode = compile(file.read(), os.path.join(dirname, path), mode="exec")
            exec(bytecode, file_globals)
            if file_globals["check"](extracted_paths, package):
                possible_templates.append(
                    (
                        os.path.splitext(os.path.basename(path))[0],
                        file_globals["template"],
                    )
                )

    template_indices = [0]
    if not possible_templates:
        raise RuntimeError("No templates apply to this package!")
    if len(possible_templates) > 1:
        print("The following templates matched:")
        for template, _ in possible_templates:
            print(template)

        template_indices = prompt_num_multi(
            "Multiple templates matched, Which templates do you want to apply? (some may not be designed to work with others)",
            max_val=len(possible_templates),
        )

    if possible_templates:
        for value in template_indices:
            possible_templates[value][1](extracted_paths, package)

    def get_header_start_year(line: str):
        match = re.search(r"Copyright (\d{4})", line)
        if match:
            return match.group(0)

    def is_end_year_correct(line: str, year: int):
        return re.search(r"Copyright \d{4}-" + str(year), line)

    year = datetime.datetime.now().year
    if not pybuild or not is_end_year_correct(str(pybuild[0]), year):
        # Looks like an old copyright statement, but is not correct
        if pybuild and str(pybuild[0]).startswith("# Copyright"):
            pybuild[0:2] = get_header_string(
                year, get_header_start_year(str(pybuild[0]))
            )
        else:
            for line in reversed(get_header_string(year)):
                pybuild.insert(0, line)

    # Import statements
    imports = {}
    for i in pybuild.find("FromImportNode") or []:
        imports[".".join([str(x) for x in i.value])] = i.parent

    if imports:
        # Update imports if any imports are missing
        if "portmod.pybuild" in imports:
            imports["portmod.pybuild"].value = "pybuild"
            imports["pybuild"] = imports["portmod.pybuild"]

        for imp in package.imports:
            if imp in imports:
                for other_import in imports[imp]:
                    if not imports[imp].targets.find("name", other_import):
                        imports[imp].targets.append(other_import)
            else:
                if package.imports[imp]:
                    pybuild.insert(
                        3, f'from {imp} import {", ".join(package.imports[imp])}'
                    )
    else:
        index = 3
        for import_name in package.imports:
            if package.imports[import_name]:
                pybuild.insert(
                    index,
                    f'from {import_name} import {", ".join(package.imports[import_name])}',
                )
                index += 1

    Mod = pybuild.find("class", "Package")

    values = {
        "NAME": f'"{package.name}"',
        "DESC": f'"{package.desc}"',
        "HOMEPAGE": f'"{package.homepage}"',
        "LICENSE": f'"{package.license}"',
        # Empty keywords, but a todo in interactive mode since we assume someone will review it
        "KEYWORDS": '"TODO: test and then FILLME"'
        if os.environ.get("INTERACTIVE")
        else '""',
    }
    if package.required_use:
        values["REQUIRED_USE"] = f'"{" ".join(package.required_use)}"'

    for field in package.other_fields:
        values[field] = package.other_fields[field]

    if Mod:
        # Add missing superclasses
        for superclass in package.classes:
            if not Mod.inherit_from.find("name", superclass):
                Mod.inherit_from.insert(0, superclass)

        # Make sure keywords are cleared
        # if they already exist in the previous version
        # This is a generated package, and is untested
        clear_keywords(pybuild)

        if "NEXUS_SRC_URI" in package.other_fields:
            if Mod.find("name", "NEXUS_SRC_URI"):
                oldvalue = str(Mod.find("name", "NEXUS_SRC_URI").parent.value)
                for source in package.sources:
                    if source.name not in oldvalue:
                        Mod.find(
                            "name", "NEXUS_SRC_URI"
                        ).parent.value = f'{package.other_fields["NEXUS_SRC_URI"]}'
                        break
            else:
                print("Removing SRC_URI and NEXUS_URL")

                def remove_field(name):
                    node = Mod.find("name", name).parent
                    del node.parent[node.index_on_parent]

                remove_field("SRC_URI")
                remove_field("NEXUS_URL")
                Mod.append(f'NEXUS_SRC_URI={package.other_fields["NEXUS_SRC_URI"]}')
        else:
            # Update SRC_URI unless there are no missing files
            if Mod.find("name", "SRC_URI"):
                old_value = Mod.find("name", "SRC_URI").parent.value

                # Keep old SRC_URI if the old value included substitutions
                # Since it's probably set up to be version independent
                if not re.search("{.*}", str(old_value)) and package.src_uri:
                    for filename in package.src_uri.split():
                        if filename not in str(old_value):
                            Mod.find(
                                "name", "SRC_URI"
                            ).parent.value = f'"{package.src_uri}"'
                            break
            else:
                Mod.append(f'SRC_URI="{package.src_uri}"')

        # Update S if present
        if Mod.find("name", "S", recursive=False) and len(package.sources) == 1:
            source_name, _ = os.path.splitext(package.sources[0].name)
            if source_name.endswith(".tar"):
                source_name, _ = os.path.splitext(source_name)
            Mod.find("name", "S", recursive=False).parent.value = f'"{source_name}"'

        # Add missing variables to mod
        for key in values:
            if not Mod.find("name", key):
                Mod.append(f"{key}={values[key]}")
                print("Adding value", key)
    else:
        valuestr = "\n    ".join([f"{key}={value}" for key, value in values.items()])
        pybuild.append(
            f'class Package({", ".join(reversed(package.classes))}):\n    {valuestr}'
        )
        Mod = pybuild.find("class", "Package")
        if package.src_uri:
            Mod.append(f'SRC_URI="{package.src_uri}"')

    # Update existing variables in values
    for field, field_value in values.items():
        if Mod.find("name", field):
            node = Mod.find("name", field)
            if isinstance(field_value, str):
                node.parent.value = field_value
            elif isinstance(field_value, list):
                elems = [
                    node
                    for node in Mod.find("name", field).parent.value
                    if isinstance(node, AtomtrailersNode)
                ]

                if len(elems) == 1 and len(field_value) == 1:
                    update_namespace(elems[0], field_value[0])
                else:
                    pending_dirs = {
                        getattr(ns, get_first_attr(ns)): ns for ns in field_value
                    }
                    pending_dirs.update(
                        {getattr(ns, get_first_attr(ns)): ns for ns in field_value}
                    )
                    for node in elems:
                        if isinstance(node, AtomtrailersNode):
                            # Assume values are identified uniquely by their first argument
                            idir = node.value[1]
                            path = idir.value[0]
                            entirepath = str(path.value).strip('"')

                            # Try to find dir in INSTALL_DIRS for new mod that matches.
                            # This is hard because S has probably changed
                            # If S is not specified, it is easier, but the PATH may have changed
                            # We could attempt to match based on other fields, but the simplest
                            # and most reliable way is to throw out old code
                            # and create a new InstallDir
                            if entirepath in pending_dirs:
                                update_namespace(node, pending_dirs[entirepath])
                                del pending_dirs[entirepath]
                            else:
                                # If none exists, we remove this node
                                elems.remove(node)

                    # Add missing new directories
                    dirlist = Mod.find("name", "INSTALL_DIRS").parent.value
                    for new_dir in pending_dirs.values():
                        dirlist.value.append(RedBaron(str(new_dir)))

    build_file: str = pybuild.dumps()

    print(build_file)

    print("Sorting imports...")
    build_file = isort.code(build_file)
    print("Formatting code...")
    build_file = black.format_str(build_file, mode=black.FileMode())
    return build_file


def create_metadata(
    path: str,
    authors: Iterable[str] = (),
    longdescription: Optional[str] = None,
    bugs_to: Optional[str] = None,
    doc: Optional[str] = None,
    maintainer: Optional[str] = None,
    changelog: Optional[str] = None,
    sources: Iterable[PackageSource] = (),
    tags: Iterable[str] = (),
):
    yaml = YAML(typ="rt")  # default, if not specfied, is 'rt' (round-trip)
    if os.path.exists(path):
        with open(path) as file:
            metadata = yaml.load(file) or yaml.map()
    else:
        metadata = yaml.map()

    if longdescription:
        metadata["longdescription"] = longdescription
    if maintainer:
        metadata["maintainer"] = maintainer

    if (authors or bugs_to or doc) and not metadata.get("upstream"):
        metadata["upstream"] = {}
    if authors and not metadata["upstream"].get("maintainer"):
        metadata["upstream"]["maintainer"] = authors
    if bugs_to:
        metadata["upstream"]["bugs-to"] = bugs_to
    if doc:
        metadata["upstream"]["doc"] = doc
    if changelog:
        metadata["upstream"]["changelog"] = changelog
    if tags:
        metadata["tags"] = tags

    for source in sources:
        source_metadata = source.get_metadata()
        if source_metadata is not None:
            if "updates" not in metadata:
                metadata["updates"] = {}
            if "source" not in metadata["updates"]:
                metadata["updates"]["source"] = []
            if source_metadata not in metadata["updates"]["source"]:
                metadata["updates"]["source"].append(source_metadata)

    with open(path, "w") as file:
        yaml.dump(metadata, file)


def update_files_in_repo(repo: str):
    """
    Yields objects which can be used to modify all packages in the repository

    The objects have the same attributes as a Pybuild, but are actually a special
    wrapper class which tracks changed attributes.
    If changes are made to the object, the file will be overwritten with the changes.
    Ensure that any field values produce valid python with ``repr``.
    """
    get_repo_root(repo)
    for pkg in load_all(only_repo_root=repo):
        with update_package(pkg) as pkg:
            yield pkg


@contextmanager
def update_package(pkg: Pybuild):
    """
    A contextmanager for updating values in a package.

    Will overwrite pkg.FILE with any changes made.
    """

    class Wrapper(SimpleNamespace):
        def __init__(self, **kwargs):
            for key, value in kwargs.items():
                super().__setattr__(key, value)
            super().__setattr__("_values", {})

        def __setattr__(self, key, value):
            self._values[key] = value
            super().__setattr__(key, value)

    updates = Wrapper(**pkg.__dict__)
    try:
        yield updates
    finally:
        # Code to release resource, e.g.:
        with open(pkg.FILE, "r", encoding="utf-8") as pybuild_file:
            pybuild = RedBaron(pybuild_file.read())

        package = pybuild.find("class", "Package")
        for key, value in updates._values.items():
            if not package.find("name", key):
                package.append(f"{key}={value}")
            else:
                package.find("name", key).parent.value = repr(value)

        if updates._values:
            build_file: str = pybuild.dumps()
            build_file = black.format_str(build_file, mode=black.FileMode())
            with open(pkg.FILE, "w", encoding="utf-8") as file:
                print(build_file, file=file, end="")


def get_value(node):
    import redbaron.nodes

    if isinstance(node, redbaron.nodes.StringNode):
        return node.to_python()
    elif isinstance(node, redbaron.nodes.NameNode):
        if node.value in ("True", "False"):
            return node.to_python()
        return "${" + str(node.value) + "}"
    elif isinstance(node, redbaron.nodes.ListNode):
        return list(get_value(elem) for elem in node.value)
    elif isinstance(node, redbaron.nodes.AtomtrailersNode):
        classname = str(node.value[0])
        if classname in ("InstallDir", "File"):
            value = get_value(node.value[1][0].value)
            if classname == "InstallDir":
                key = "PATH"
            elif classname == "File":
                key = "NAME"
            else:
                NotImplementedError()
            result = dict()
            result[key] = value
            for elem in node.value[1][1:]:
                result[str(elem.target)] = get_value(elem.value)
            return result
    raise Exception(type(node))


def to_yaml(pkg_file: str) -> ruamel.yaml.comments.CommentedMap:
    import redbaron.nodes

    if not pkg_file.endswith(".pybuild"):
        raise RuntimeError("File is not a .pybuild")

    with open(pkg_file, "r", encoding="utf-8") as pybuild_file:
        pybuild = RedBaron(pybuild_file.read())

    yaml = YAML()
    doc = yaml.map()

    imports = {}
    for i in pybuild.find_all("FromImportNode") or []:
        for target in i.targets:
            imports[str(target)] = ".".join([str(x) for x in i.value])

    for elem in pybuild:
        if isinstance(elem, redbaron.nodes.CommentNode):
            doc.yaml_set_start_comment(str(elem).lstrip("#").strip())
        if isinstance(elem, redbaron.nodes.AssignmentNode):
            doc[str(elem.target)] = get_value(elem.value)
        elif isinstance(elem, redbaron.nodes.ClassNode):
            assert elem.name == "Package"
            comment = None
            # Find inherited classes
            superclasses = []
            for name in elem.inherit_from[0]:
                superclasses.append(imports[str(name)] + ":" + str(name))

            doc["inherit"] = superclasses
            for child in elem:
                if isinstance(child, redbaron.nodes.AssignmentNode):
                    doc.insert(
                        len(doc),
                        str(child.target),
                        get_value(child.value),
                        comment=comment,
                    )
                    comment = None
                elif isinstance(child, redbaron.nodes.CommentNode):
                    comment = str(child)
    return cast(ruamel.yaml.comments.CommentedMap, doc)
