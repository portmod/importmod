import json
import os
from types import SimpleNamespace
from typing import List

from ruamel.yaml import YAML

from importmod.atom import parse_version
from importmod.sources import PackageData


class Install(SimpleNamespace):
    path: str
    rename: str


def check(extracted_archives: List[str], package: PackageData) -> bool:
    for archive in extracted_archives:
        for root, directories, files in os.walk(archive):
            for directory in directories:
                if directory.lower() == "bepinex":
                    return True
            for file in files:
                if file.lower() == "swinfo.json":
                    return True
    return False


def template(extracted_archives: List[str], package: PackageData):
    package.imports["common.ckan"] |= {"CKAN", "Install"}
    package.classes.append("CKAN")
    install_root = None
    rename = None
    for archive in extracted_archives:
        for root, directories, files in os.walk(archive):
            for directory in directories:
                if directory.lower() == "bepinex":
                    install_root = os.path.relpath(root, archive)
    found_netkan = False

    for archive in extracted_archives:
        for root, directories, files in os.walk(archive):
            for path in files:
                if path.endswith(".netkan"):
                    with open(os.path.join(root, path), encoding="utf-8") as file:
                        yaml = YAML()
                        data = yaml.load(file)
                    # Netkan should be stable enough to automatically set as testing
                    package.other_fields["KEYWORDS"] = '"~ksp2"'
                    package.atom = data["identifier"]
                    package.license = data.get("license", package.license)

                    found_netkan = True
                    install_root = None

                    INSTALL = package.other_fields["INSTALL"] = []
                    # FIXME: This is more complicated. It might be worthwhile to update common/ckan to match more precisely.
                    for install in data["install"]:
                        INSTALL.append(Install(*install))

                    # TODO: Version-specific dependency parsing. Can be borrowed from ckanconvert
                    package.other_fields["RDEPEND"] = (
                        '"'
                        + " ".join(depend["name"] for depend in data["depends"])
                        + '"'
                    )
                    package.tags += data.get("tags", [])

            if "swinfo.json" in files and not found_netkan:
                if not install_root:
                    install_root = os.path.relpath(root, archive)
                    rename = f"BepInEx/Plugins/{package.atom.PN}"
                with open(os.path.join(root, "swinfo.json"), encoding="utf-8") as file:
                    info = json.load(file)
                package.other_fields["ksp2_version"] = info["ksp2_version"]
                if "author" in info:
                    package.authors = info.get("author")
                package.desc = info.get("description", package.desc)
                package.other_fields["RDEPEND"] = (
                    '"' + " ".join(info["dependencies"] + ["dev-util/spacewarp"]) + '"'
                )
                package.update_atom(parse_version(info["version"]))

    if install_root:
        package.other_fields["INSTALL"] = [
            Install(path=f"{install_root}", rename=rename)
        ]
