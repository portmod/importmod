import os
from types import SimpleNamespace
from typing import List, Set

from portmodlib.atom import Atom

from importmod.deps import get_masters
from importmod.sources import PackageData
from importmod.util import tr_patcher
from importmod_core.datadir import (
    find_data_dirs,
    find_esp_bsa,
    get_dominant_texture_size,
)
from importmod_core.install import select_data_dirs


class File(SimpleNamespace):
    NAME: str


def check(extracted_archives: List[str], package: PackageData) -> bool:
    for archive in extracted_archives:
        if find_data_dirs(archive):
            return True
    return False


def template(extracted_archives: List[str], package: PackageData):
    package.imports["common.mw"].add("MW")
    package.classes.append("MW")
    INSTALL_DIRS = package.other_fields["INSTALL_DIRS"] = []
    TEXTURE_SIZES = set()
    build_deps: Set[Atom] = set()
    dep_atoms: Set[Atom] = set()
    dep_uses: Set[str] = set()

    for archive in extracted_archives:
        # Search for data directories
        # Ignore esps and BSAs for now, we need them for each data directory
        dirs, _, _ = select_data_dirs(archive)

        for directory in dirs:
            (esps, bsas) = find_esp_bsa(os.path.join(archive, directory.PATH))
            if bsas:
                directory.ARCHIVES = [File(NAME=bsa) for bsa in bsas]

            source_name, _ = os.path.splitext(os.path.basename(archive))
            if source_name.endswith(".tar"):
                source_name, _ = os.path.splitext(source_name)

            texture_size = get_dominant_texture_size(
                os.path.join(archive, directory.PATH)
            )

            if texture_size:
                TEXTURE_SIZES.add(texture_size)

            PLUGINS = []
            # Get dependencies for the ESP.
            for esp in esps:
                esp_path = os.path.join(archive, directory.PATH, esp)
                TR_PATCH = False
                if not esp.lower().endswith(".omwscripts"):
                    print(f"Masters of esp {esp} are {get_masters(esp_path)}")

                    if "TR_Data.esm" in get_masters(esp_path):
                        TR_PATCH = True
                        package.imports["common.util"].add("TRPatcher")
                        if "TRPatcher" not in package.classes:
                            package.classes.insert(0, "TRPatcher")
                        print(f"TR Patching file {esp}")
                        tr_patcher(esp_path)

                plugin = File(NAME=esp)

                if TR_PATCH:
                    plugin.TR_PATCH = True

                PLUGINS.append(plugin)

            if PLUGINS:
                directory.PLUGINS = PLUGINS

            if len(package.sources) > 1:
                directory.S = source_name

            INSTALL_DIRS.append(directory)

    if "base/morrowind" in dep_atoms and dep_uses:
        dep_atoms.remove("base/morrowind")
        dep_atoms.add("base/morrowind[" + ",".join(sorted(dep_uses)) + "]")

    deps = " ".join(sorted(dep_atoms))

    if len(TEXTURE_SIZES) > 1:
        package.other_fields["TEXTURE_SIZES"] = (
            '"' + " ".join(map(str, sorted(TEXTURE_SIZES))) + '"'
        )

    if INSTALL_DIRS:
        package.imports["common.mw"].add("InstallDir")
    if any(
        list(
            getattr(d, attr)
            for attr in ["PLUGINS", "ARCHIVES", "GROUNDCOVER"]
            if hasattr(d, attr)
        )
        for d in INSTALL_DIRS
    ):
        package.imports["common.mw"].add("File")

    if deps:
        package.other_fields["RDEPEND"] = f'"{deps}"'
    if build_deps:
        package.other_fields["DEPEND"] = '"' + " ".join(sorted(build_deps)) + '"'
