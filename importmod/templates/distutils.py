import os
from logging import warning
from typing import List

import tomlkit

from importmod.sources import PackageData


def check(extracted_archives: List[str], package: PackageData) -> bool:
    setup_py_found = False
    pep517 = False
    for archive in extracted_archives:
        for root, _, files in os.walk(archive):
            for filename in files:
                if filename == "setup.py":
                    setup_py_found = True
                if filename == "pyproject.toml":
                    with open(os.path.join(root, filename), encoding="utf-8") as file:
                        data = tomlkit.load(file)
                    if data.get("build-backend"):
                        pep517 = True

    return setup_py_found and not pep517


def template(extracted_archives: List[str], package: PackageData):
    package.imports["common.distutils"] |= {"Distutils"}
    package.classes.append("Distutils")
    # FIXME: Category currently won't apply, as it is required before this runs
    package.category = "dev-python"

    warning(
        "The distutils template cannot detect dependencies or metadata from setup.py!"
    )

    for archive in extracted_archives:
        for root, _, files in os.walk(archive):
            for filename in files:
                if filename == "setup.py":
                    package.other_fields["S"] = (
                        '"' + os.path.relpath(root, os.path.dirname(archive)) + '"'
                    )
