# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import re
from logging import info, warning
from typing import Optional

from fuzzywuzzy import fuzz
from portmod.repo import Repo
from portmod.repo.metadata import get_masters

REPOS_SEEN = set()
LICENSES = {}


def _normalise_license(path: str):
    with open(path, encoding="utf-8") as file:
        text = file.readlines()

    # Title line
    if text[0].lower().endswith("license"):
        del text[0]

    for index, line in reversed(list(enumerate(text))):
        # Strip out copyright statements
        if line.lower().startswith("copyright"):
            del text[index]
    return re.sub(r"\s+", " ", " ".join(text).lower().strip())


def _collect_licenses(repo: Repo):
    if repo.location in REPOS_SEEN:
        return
    REPOS_SEEN.add(repo.location)
    path = os.path.join(repo.location, "licenses")
    if os.path.exists(path):
        for license in os.listdir(path):
            LICENSES[_normalise_license(os.path.join(path, license))] = license
    for master in get_masters(repo.location):
        _collect_licenses(master)


def detect_license(repo: Repo, path: str) -> Optional[str]:
    _collect_licenses(repo)

    normalised = _normalise_license(path)
    ratings = []
    for license, name in LICENSES.items():
        ratings.append((fuzz.ratio(license, normalised), name))

    best_rating, best = max(ratings, key=lambda x: int(x[0]))
    if best_rating > 75:
        info(f"Detected license {best} with confidence {best_rating}/100")
        info(f"From file {path}")
        return best
    warning(
        f"Best license detected was license {best} with confidence {best_rating}/100"
    )
    warning("Rating is low, should be double-checked")
    return None
