# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

"""Utility functions for interacting with mw.modhistory.com"""

import json
import urllib.request
from typing import Any, Dict, List

import requests
from portmodlib.source import Source

from ..atom import parse_atom, parse_version
from . import PackageData, PackageSource, Update


class SpaceDockSource(PackageSource):
    def __init__(self, idnum: int):
        self.idnum = idnum
        self.info = None
        self.name = None

    def get_info(self):
        if self.info is None:
            self.info = get_spacedock_info(self.get_api_url())
            assert self.info is not None
            self.name = self.info["name"]

    def get_url(self) -> str:
        self.get_info()
        assert self.name is not None
        return f"https://spacedock.info/mod/{self.idnum}/{urllib.request.pathname2url(self.name)}"

    def get_api_url(self):
        return f"https://spacedock.info/api/mod/{self.idnum}"

    def get_newest_version(self) -> str:
        self.get_info()
        assert self.info is not None
        return parse_version(self.info["versions"][0]["friendly_version"])

    def __hash__(self):
        return hash(f"spacedock/{self.idnum}")

    def get_pkg_data(self, package: PackageData):
        self.get_info()
        assert self.info is not None
        info = self.info
        newest_version = info["versions"][0]

        package.homepage = self.get_url()
        if info.get("source_code"):
            package.homepage += " " + info["source_code"]
        package.name = info["name"]
        package.atom = parse_atom(
            package.name + "-" + parse_version(newest_version["friendly_version"])
        )
        package.other_fields["game_version"] = f'"{newest_version["game_version"]}"'
        package.changelog = self.get_url() + "#changelog"
        package.desc = info["short_description"]
        package.authors.append(info["author"])
        package.license = info["license"]

        # Note: regular description is markdown and may include links
        # Disabled since it's usually too verbose
        # from bs4 import BeautifulSoup
        # soup = BeautifulSoup(info["description_html"])
        # package.longdescription = soup.get_text()

        package.sources = [
            Source(
                "https://spacedock.info" + newest_version["download_path"],
                package.atom.P + ".zip",
            )
        ]
        package.src_uri = f"https://spacedock.info{newest_version['download_path']} -> {package.atom.P}.zip"
        return package

    def get_metadata(self) -> Dict[str, Any]:
        """Returns metadata suitable for inclusion in metadata.yaml to be used to detect updates"""
        return {"type": "spacedock", "idnum": self.idnum}


def get_spacedock_info(url):
    """
    Returns information in the hompage for the given modhistory mod

    Generally contains the following fields:
    Version, Added, Last Edited, File Size, Downloads, Requires, Submitted by
    """
    rinfo = requests.get(url)
    if rinfo.status_code != requests.codes.ok:  # pylint: disable=no-member
        rinfo.raise_for_status()

    return json.loads(rinfo.text)


def get_spacedock_updates(period: str, mod_map) -> List[Update]:
    """
    Returns a list of updates to nexusmods.com mods in the given period

    Valid periods are 1d, 1w, 1m
    """
    assert period in ["1d", "1w", "1m"]
    page = 0
    while True:
        update_url = f"https://spacedock.info/api/browse?page={page}&orderby=updated"

    uinfo = requests.get(update_url)

    if uinfo.status_code == requests.codes.ok:  # pylint: disable=no-member
        info = json.loads(uinfo.text)
    else:
        uinfo.raise_for_status()

    updates = []
    for mod in info["result"]:
        source = SpaceDockSource(mod["id"])
        if source in mod_map:
            update = source.get_update(mod_map[source])
            if update:
                updates.append(update)
    return updates
