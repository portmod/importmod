# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from tempfile import TemporaryDirectory

from importmod_core.io import insert_sorted


def test_atom():
    """Basic atom parsing test"""
    with TemporaryDirectory() as tempdir:
        path = os.path.join(tempdir, "file")
        with open(path, "w") as file:
            print("bar", file=file)
            print("foo", file=file)
            print("zaf", file=file)
        insert_sorted(path, "value")

        with open(path) as file:
            assert list(map(lambda x: x.strip(), file.readlines())) == [
                "bar",
                "foo",
                "value",
                "zaf",
            ]
        os.remove(path)
        insert_sorted(path, "value")
        with open(path) as file:
            assert list(map(lambda x: x.strip(), file.readlines())) == [
                "value",
            ]
